package com.hps.privilegemanager.repository;

import com.hps.privilegemanager.entity.User;
import com.hps.privilegemanager.vo.UserVo;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    public void testSave() {
        User user = new User();
        user.setName("李四");
        user.setPwd("12345678");
        userRepository.save(user);
        System.out.println(user.getId());
    }

    @Test
    public void testFind() throws IllegalAccessException {
        ArrayList<Long> ids = new ArrayList<>();
        ids.add(1L);
        ids.add(2L);
        List<User> users = userRepository.findAll(ids);
        System.out.println(users.size());
        Field[] fields = User.class.getDeclaredFields();
        for(User user: users) {
            for(Field field: fields) {
                field.setAccessible(true);
                System.out.print(field.getName() + ": " + field.get(user) + " ");
            }
            System.out.println();
        }
    }

    @Test
    public void testGetMap() {
        Query query = entityManager.createNativeQuery("select name, pwd from auth_user");
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        List<Map<String, Object>> rows = query.getResultList();

        List<Map<String, Object>> mapList = userRepository.getMap();
        System.out.println(mapList.size());
        for(Map<String, Object> object: mapList) {
            Map map = object;
            System.out.println(map.get("name") + ": " + map.get("pwd"));
        }
    }

    @Test
    public void testQuery() {
        UserVo userVo = userRepository.query();
        System.out.println(userVo.getName());
    }
}