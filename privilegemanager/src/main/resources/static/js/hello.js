var myApp = angular.module('myApp', []);
myApp.controller('myCtrl', function($scope, $http) {
    $scope.submit = function () {
        $http.post("/user/getAll", {'id':1, name: '张三'}).success(function (data) {
            $scope.userList = data;
        }).error(function (response) {
            console.log(response)
        });
    };

    $scope.addUser = function () {
        $.ajax({
            type: 'POST',
            url: "/user/addUser",
            data: $('#userForm').serialize(),
            success: function (data) {
                console.log(data);
            },
            dataType: "JSON"
        })
    }

    $scope.clear = function () {
        $scope.userList = null;
    }
});