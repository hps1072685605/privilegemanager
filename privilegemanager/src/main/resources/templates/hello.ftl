<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="css/hello.css">
    <script src="js/angular.min.js"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/hello.js"></script>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<h1>Hi</h1>
Hello ok, ${age}
<button id="btn" ng-click="submit()">提交</button>&nbsp;
<button id="btn-clear" ng-click="clear()">清空</button>
<table>
    <thead>
      <tr>
          <td>id</td>
          <td>用户名</td>
          <td>密码</td>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="user in userList">
          <td>{{user.id}}</td>
          <td>{{user.name}}</td>
          <td>{{user.pwd}}</td>
      </tr>
    </tbody>
</table>

<form id="userForm">
    <label for="name">姓名:</label><input type="text" name="name" id="name"><br>
    <label for="pwd">密码:</label><input type="password" name="pwd" id="pwd"><br>
    <button id="btn-add" ng-click="addUser()">新增用户</button>
</form>
</body>
</html>