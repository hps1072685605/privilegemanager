package com.hps.privilegemanager.repository;

import com.hps.privilegemanager.entity.User;
import com.hps.privilegemanager.vo.UserVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface UserRepository extends JpaRepository<User, Long> {
    @Query("select new map(u.name as name, u.pwd as pwd) from User u")
    List<Map<String, Object>> getMap();

    @Query("select new com.hps.privilegemanager.vo.UserVo(u.name, u.pwd) from User u where u.id = 1")
    UserVo query();
}
