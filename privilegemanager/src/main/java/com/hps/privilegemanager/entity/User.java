package com.hps.privilegemanager.entity;

import javax.persistence.*;

@Entity
@Table(name = "auth_user")
public class User {
    private Long id;
    private String name;
    private String pwd;

    @Id
    @GeneratedValue
    @Column(length = 20)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(length = 64, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(length = 64, nullable = false)
    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
