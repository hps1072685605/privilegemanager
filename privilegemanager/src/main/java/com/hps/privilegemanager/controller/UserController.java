package com.hps.privilegemanager.controller;

import com.hps.privilegemanager.entity.User;
import com.hps.privilegemanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/getAll")
    public List<User> getAll(@RequestBody User user) {
        System.out.println(user.getName());
        return userService.findAll();
    }

    @RequestMapping("/addUser")
    public Map<String, Object> addUser(User user) {
        System.out.println(user.getName());
        userService.addUser(user);
        Map<String, Object> result = new HashMap<>();
        result.put("msg", "添加成功");
        return result;
    }
}
