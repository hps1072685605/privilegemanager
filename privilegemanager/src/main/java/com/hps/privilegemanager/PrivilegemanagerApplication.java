package com.hps.privilegemanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrivilegemanagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrivilegemanagerApplication.class, args);
	}
}
